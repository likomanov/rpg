﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG1.Model
{
    public class Mage
    {
        public int Strenght { get; set; } = 2;
        public int Agility { get; set; } = 1;
        public int Intelligence { get; set; } = 3;
        public int Range { get; set; } = 3;
    }
}
