﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG1.Model
{
    public class Hero
    {
        [Key]
        public int Id { get; set; }
        public int Strenght { get; set; }
        public int Agility { get; set; }
        public int Intelligence { get; set; }
        public int Range { get; set; }
        public DateTime DateTime { get; set; }


        public Hero()
        {

        }
    }
}
