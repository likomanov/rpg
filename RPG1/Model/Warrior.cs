﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG1.Model
{
    public class Warrior
    {
        public int Strenght { get; set; } = 3;
        public int Agility { get; set; } = 3;
        public int Intelligence { get; set; } = 0;
        public int Range { get; set; } = 1;
    }
}
