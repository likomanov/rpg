﻿using RPG1.Data;
using RPG1.Display;
using RPG1.Display.Board;
using RPG1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG1
{
    public class Startup
    {
        static void Main(string[] args)
        {
            LoadingPage.Loading();

            InGame();
        }

        public static void InGame()
        {

            var hero = CharacterSelect.GetHero();
            Board board = new Board(hero);
            board.GetHealth();
            board.GetMana();
            board.DisplayBoard();
        }
    }
}
