﻿using Microsoft.EntityFrameworkCore;
using RPG1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG1.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Hero> Heroes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //please add information for your SQL here
            optionsBuilder.UseSqlServer("Data Source=DESKTOP-U5RFO5E;Initial Catalog=RPG1;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        }
    }
}
