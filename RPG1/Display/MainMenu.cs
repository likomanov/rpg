﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG1.Display
{
    public class MainMenu
    {
        public static string Menu()
        {
            Console.WriteLine("Welcome!");
            Console.WriteLine("Press any key to play.");
            string inputStart = "Begin!";
            Console.ReadKey(true);
            Console.Clear();
            CharacterSelect.Select();

            return inputStart;
        }
    }
}
