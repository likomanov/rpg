﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG1.Display.Board
{

    public class Occupation
    {
        public const char HERO_SYMBOL = '@';
        public const char SPACE = '▒';
        public const char MONSTER_SYMBOL = '◙';
        public static char[,] heroes;
        public static char[,] monsters;


        public Occupation()
        {
            heroes = new char[Board.DIMENSION, Board.DIMENSION];
            InitializeHero();
        }

        public void InitializeHero()
        {
            for (int row = 0; row < Board.DIMENSION; row++)
            {
                for (int col = 0; col < Board.DIMENSION; col++)
                {
                    if (row == 0 && col == 0)
                    {
                        heroes[0, 0] = HERO_SYMBOL;
                    }
                    else
                    {
                        heroes[row, col] = SPACE;
                    }
                }
            }
        }

        public void UpdateHero()
        {
            List<string> emptyCells = new List<string>();

            for (int row = 0; row < Board.DIMENSION; row++)
            {
                for (int col = 0; col < Board.DIMENSION; col++)
                {
                    if (row == 0 && col == 0)
                    {
                        heroes[0, 0] = HERO_SYMBOL;
                    }
                    else
                    {
                        heroes[row, col] = SPACE;
                        emptyCells.Add($"{row}, {col}");
                    }
                }
            }

            int range = emptyCells.Count;

        }
   
    }
}
