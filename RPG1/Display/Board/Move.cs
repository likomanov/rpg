﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG1.Display.Board
{

    public class Move : Occupation
    {
        public int targetX;
        public int targetY;
        public int destinationX;
        public int destinationY;
        private int command;

        public Move()
        {
            targetX = 0;
            targetY = 0;
            destinationX = 0;
            destinationY = 0;
            command = 0;
            Exit = false;
        }

        public bool Exit { get; set; }

        public void MakeMove()
        {
            GetInput();

            if (!Exit)
            {
                RearangeHero();
            }
        }

        private void GetInput()
        {
            Console.WriteLine("Choose action");
            Console.WriteLine("1) Attack");
            Console.WriteLine("2) Move");
            Exit = ValidateCommand(int.TryParse(Console.ReadLine(), out command));
        }

        private bool ValidateCommand(bool parsed)
        {
            bool error = false;

            if (!parsed)
            {
                error = true;
            }
            else if (command != 1 && command != 2)
            {
                error = true;
            }
            else if (command == 2)
            {
                CheckMove();
            }
            if (error)
            {
                Console.WriteLine("Invalid command!");
            }
            return error;
        }

        private void CheckMove()
        {
            Console.WriteLine("Please make a move:");
            string move = Console.ReadLine();
            if (move.Length > 1)
            {
                Console.WriteLine("Invalid input!");
            }
            else
            {
                switch (move)
                {
                    case "w":
                        if (targetX > 0)
                        {
                            targetX--;
                            base.UpdateHero();
                        }
                        else
                        {
                            Console.WriteLine("Invalid input!");
                        }
                        break;
                    case "s":
                        if (targetX < 9)
                        {
                            targetX++;
                            base.UpdateHero();
                        }
                        else
                        {
                            Console.WriteLine("Invalid input!");
                        }
                        break;
                    case "d":
                        if (targetY < 9)
                        {
                            targetY++;
                            base.UpdateHero();
                        }
                        else
                        {
                            Console.WriteLine("Invalid input!");
                        }
                        break;
                    case "a":
                        if (targetY > 0)
                        {
                            targetY--;
                            base.UpdateHero();
                        }
                        else
                        {
                            Console.WriteLine("Invalid input!");
                        }
                        break;
                    case "e":
                        if (targetY < 9 && targetX < 9)
                        {
                            targetY++;
                            targetX--;
                            base.UpdateHero();
                        }
                        break;
                    case "x":
                        if (targetY < 9 && targetX > 0)
                        {
                            targetY++;
                            targetX++;
                            base.UpdateHero();
                        }
                        break;
                    case "q":
                        if (targetX > 0 && targetY > 0)
                        {
                            targetX--;
                            targetY--;
                            base.UpdateHero();
                        }
                        break;
                    case "z":
                        if (targetY > 0 && targetX < 9)
                        {
                            targetY--;
                            targetX++;
                            base.UpdateHero();
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void RearangeHero()
        {
            char temp = heroes[destinationX, destinationY];
            heroes[destinationX, destinationY] = heroes[targetX, targetY];
            heroes[targetX, targetY] = temp;
        }
    }
}
