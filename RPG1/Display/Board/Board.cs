﻿using RPG1.Data;
using RPG1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RPG1.Display.Board
{
    public class Board
    {
        public string[,] board;
        public const int DIMENSION = 10; // 10x10 board

        private Move move;
        private int health = 0;
        private int mana = 0;
        private Hero hero;

        public Board(Hero hero)
        {
            move = new Move();
            board = new string[DIMENSION, DIMENSION];
            boardSymbol = "▒";
            this.hero = hero;
        }

        public string boardSymbol { get; set; }

        public void GetHealth()
        {
            this.health = this.hero.Strenght * 5;
        }

        public void GetMana()
        {
            this.mana = this.hero.Intelligence * 3;
        }

        public void DisplayBoard()
        {
            while (!move.Exit)
            {
                Console.Clear();
                Console.WriteLine($"Health:{this.health}  Mana:{this.mana}");
                Console.WriteLine();
                for (int row = 0; row < DIMENSION; row++)
                {
                    for (int col = 0; col < DIMENSION; col++)
                    {
                        board[row, col] = boardSymbol;

                        Console.Write(boardSymbol + Occupation.heroes[row, col]);
                    }
                    Console.WriteLine();
                }

                move.MakeMove();
            }
        }
    }
}
