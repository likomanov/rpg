﻿using Microsoft.EntityFrameworkCore.Update.Internal;
using RPG1.Data;
using RPG1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace RPG1.Display
{
    public class CharacterSelect
    {
        private static Hero heroToUse;

        public static string Select()
        {
            bool hasPicked = false;
            int pointsToAdd = 3;

            Console.WriteLine("Choose character type:");
            Console.WriteLine();
            Console.WriteLine("Options:");
            Console.WriteLine();
            Console.WriteLine("1) Warrior");
            Console.WriteLine();
            Console.WriteLine("2) Archer");
            Console.WriteLine();
            Console.WriteLine("3) Mage");
            Console.WriteLine();
            Console.WriteLine("Your pick:");

            string inputStart = Console.ReadLine();
            Console.WriteLine();

            int raceIndex = 0;
            while (hasPicked == false)
            {
                switch (inputStart)
                {
                    case "1":
                        Console.WriteLine("You have selected Warrior.");
                        raceIndex = 1;
                        hasPicked = true;
                        break;
                    case "2":
                        Console.WriteLine("You have selected Archer.");
                        raceIndex = 2;
                        hasPicked = true;
                        break;
                    case "3":
                        Console.WriteLine("You have selected Mage.");
                        raceIndex = 3;
                        hasPicked = true;
                        break;
                    default:
                        Console.WriteLine("Invalid input!");
                        inputStart = Console.ReadLine();
                        break;
                }
            }

            if (raceIndex == 1)
            {
                Warrior warrior = new Warrior();
                Hero hero = new Hero
                {
                    Strenght = warrior.Strenght,
                    Agility = warrior.Agility,
                    Intelligence = warrior.Intelligence,
                    Range = warrior.Range,
                };
                LoadHero(hero);
            }
            else if (raceIndex == 2)
            {
                Archer archer = new Archer();
                Hero hero = new Hero
                {
                    Strenght = archer.Strenght,
                    Agility = archer.Agility,
                    Intelligence = archer.Intelligence,
                    Range = archer.Range,
                };
                LoadHero(hero);
            }
            else if (raceIndex == 3)
            {
                Mage mage = new Mage();
                Hero hero = new Hero
                {
                    Strenght = mage.Strenght,
                    Agility = mage.Agility,
                    Intelligence = mage.Intelligence,
                    Range = mage.Range,
                };
                LoadHero(hero);
            }
            Console.WriteLine();
            Console.WriteLine("Would you like to buff up your stats before starting?             (Limit: 3 points total)");
            Console.WriteLine("Response (Y\\N):");


            string addPoints = Console.ReadLine();
            hasPicked = false;
            bool wantsToBuff = false;

            while (hasPicked == false)
            {
                switch (addPoints)
                {
                    case "y":
                        hasPicked = true;
                        wantsToBuff = true;
                        break;
                    case "n":
                        Startup.InGame();
                        hasPicked = true;
                        break;

                    default:
                        Console.WriteLine("Invalid input!");
                        addPoints = Console.ReadLine();
                        break;
                }
            }

            if (wantsToBuff)
            {
                Console.WriteLine($"Remaining Points: {pointsToAdd}");
                Console.WriteLine($"Add to Strenght:");
                bool breakWhile = false;

                while (breakWhile == false)
                {
                    int numericValue;
                    string input = Console.ReadLine();
                    bool isNumber = int.TryParse(input, out numericValue);

                    if (!isNumber)
                    {
                        Console.WriteLine("Invalid input!");
                    }
                    else
                    {
                        int added = int.Parse(input);
                        if (added > pointsToAdd)
                        {
                            Console.WriteLine("Invalid input!");
                        }

                        else
                        {
                            List<Hero> list = new List<Hero>();
                            using (var db = new ApplicationDbContext())
                            {
                                list = db.Heroes.ToList();
                                var lastHero = list.LastOrDefault();
                                UpdateStrenght(lastHero, added);
                            }
                            pointsToAdd -= added;
                            breakWhile = true;
                        }

                    }

                }

            }

            if (pointsToAdd == 0)
            {
                Console.WriteLine("You have no more points to add. Your game starts!");
                Startup.InGame();
            }

            else if (pointsToAdd > 0)
            {
                Console.WriteLine($"Remaining Points: {pointsToAdd}");
                Console.WriteLine($"Add to Agility:");
                bool breakWhile = false;

                while (breakWhile == false)
                {
                    int numericValue;
                    string input = Console.ReadLine();
                    bool isNumber = int.TryParse(input, out numericValue);

                    if (!isNumber)
                    {
                        Console.WriteLine("Invalid input!");
                    }
                    else
                    {
                        int added = int.Parse(input);
                        if (added > pointsToAdd)
                        {
                            Console.WriteLine("Invalid input!");
                        }

                        else
                        {
                            List<Hero> list = new List<Hero>();
                            using (var db = new ApplicationDbContext())
                            {
                                list = db.Heroes.ToList();
                                var lastHero = list.LastOrDefault();
                                UpdateAgility(lastHero, added);
                            }
                            pointsToAdd -= added;
                            breakWhile = true;
                        }

                    }

                }
            }

            if (pointsToAdd == 0)
            {
                Console.WriteLine("You have no more points to add. Your game starts!");
                Startup.InGame();
            }

            else if (pointsToAdd > 0)
            {
                Console.WriteLine($"Remaining Points: {pointsToAdd}");
                Console.WriteLine($"Add to Intelligence:");
                bool breakWhile = false;

                while (breakWhile == false)
                {
                    int numericValue;
                    string input = Console.ReadLine();
                    bool isNumber = int.TryParse(input, out numericValue);

                    if (!isNumber)
                    {
                        Console.WriteLine("Invalid input!");
                    }
                    else
                    {
                        int added = int.Parse(input);
                        if (added > pointsToAdd)
                        {
                            Console.WriteLine("Invalid input!");
                        }

                        else
                        {
                            List<Hero> list = new List<Hero>();
                            using (var db = new ApplicationDbContext())
                            {
                                list = db.Heroes.ToList();
                                var lastHero = list.LastOrDefault();
                                UpdateIntelligence(lastHero, added);
                            }
                            pointsToAdd -= added;
                            breakWhile = true;
                        }

                    }

                }
            }

            Console.WriteLine("Enjoy the game!");
            Startup.InGame();

            return inputStart;
        }

        public static void LoadHero(Hero hero)
        {
            ApplicationDbContext ob = new ApplicationDbContext();

            Hero myHero = new Hero();

            myHero.Strenght = hero.Strenght;
            myHero.Agility = hero.Agility;
            myHero.Range = hero.Range;
            myHero.Intelligence = hero.Intelligence;
            myHero.DateTime = DateTime.Now;

            heroToUse = hero;

            ob.Add(myHero);
            ob.SaveChanges();
        }

        public static void UpdateStrenght(Hero hero, int added)
        {
            ApplicationDbContext ob = new ApplicationDbContext();

            hero.Strenght = hero.Strenght + added;
            hero.Agility = hero.Agility;
            hero.Range = hero.Range;
            hero.Intelligence = hero.Intelligence;
            hero.DateTime = DateTime.Now;

            heroToUse = hero;

            ob.Update(hero);
            ob.SaveChanges();
        }

        public static void UpdateAgility(Hero hero, int added)
        {
            ApplicationDbContext ob = new ApplicationDbContext();

            hero.Strenght = hero.Strenght;
            hero.Agility = hero.Agility + added;
            hero.Range = hero.Range;
            hero.Intelligence = hero.Intelligence;
            hero.DateTime = DateTime.Now;

            heroToUse = hero;

            ob.Update(hero);
            ob.SaveChanges();
        }

        public static void UpdateIntelligence(Hero hero, int added)
        {
            ApplicationDbContext ob = new ApplicationDbContext();

            hero.Strenght = hero.Strenght;
            hero.Agility = hero.Agility;
            hero.Range = hero.Range;
            hero.Intelligence = hero.Intelligence + added;
            hero.DateTime = DateTime.Now;

            ob.Update(hero);
            ob.SaveChanges();

            heroToUse = hero;
        }

        public static Hero GetHero()
        { 
            return heroToUse;
        }
    }
}
