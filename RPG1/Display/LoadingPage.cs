﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG1.Display
{
    public class LoadingPage
    {
        public static void Loading()
        {
            Console.WriteLine("Welcome, please choose an option:");
            int userChoiceNumber = DisplayChoices();

            while (userChoiceNumber != 5)
            {
                if (userChoiceNumber == 1)
                {
                    Console.Clear();
                    MainMenu.Menu();
                    break;
                }
                else if (userChoiceNumber == 2)
                {
                    Console.Clear();
                    CharacterSelect.Select();
                    break;
                }
                else if (userChoiceNumber == 3)
                {
                    Startup.InGame();
                }
                else if (userChoiceNumber == 4)
                {
                    Environment.Exit(1);
                }
            }
        }

        static int DisplayChoices()
        {
            Console.WriteLine("1. Main menu");
            Console.WriteLine("2. CharacterSelect");
            Console.WriteLine("3. InGame");
            Console.WriteLine("4. Exit");

            string userChoice = Console.ReadLine();
            int userChoiceNumber = 0;
            while (!Int32.TryParse(userChoice, out userChoiceNumber) || userChoiceNumber < 1 || userChoiceNumber > 4)
            {
                Console.WriteLine("Invalid input!");
                userChoice = Console.ReadLine();
            }

            return userChoiceNumber;
        }
    }
}
